<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{!! asset("assets/css/bootstrap.min.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset("assets/css/style.css") !!}">
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" />
    <script src="{!! asset("assets/js/lib/jquery-2.2.0.min.js") !!}"></script>
    <script type="text/javascript" src="{!! asset("assets/js/lib/bootstrap.min.js") !!}"></script>
    <script type="text/javascript" src="{!! asset("assets/js/script.js") !!}"></script>
</head>
<body>
@include('errors.svg')
<nav class="navbar navbar-fixed-top mynav">
    <div class="container-fluid logo">
        <img src="/assets/img/logo-slc.png" style="width: 100px; margin-left: 30px;"/>
    </div>
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li>
                <a href="/">Home</a>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">Master <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/location">Location</a>
                    </li>
                    <li>
                        <a href="/campus">Campus</a>
                    </li>
                    <li>
                        <a href="/shift">Shift</a>
                    </li>
                    <li>
                        <a href="/major">Major</a>
                    </li>
                    <li>
                        <a href="/course">Course</a>
                    </li>
                    <li>
                        <a href="/ast-staff">Ast and Staff</a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="/lecture">Lecture Schedule</a>
            </li>
            <li>
                <a href="/alternate-schedule">Alternative Schedule</a>
            </li>
            <li>
                <a href="/logout">Logout</a>
            </li>
        </ul>
        <button type="button" class="navbar-toggle" data-toggle="collapse" ></button>
    </div>
</nav>

<div class="container-fluid content">
    @yield('bagian')
</div>


</body>
</html>