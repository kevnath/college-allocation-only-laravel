@extends('master.template')

@section('title', 'Insert Campus' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/insert-shift">
        {!! csrf_field() !!}
        <div class="form-group">
            <input type="text" placeholder="Shift Time" name="time" class="form-control" value="{{ old('time') }}">
        </div>

        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/course" class="btn btn-default">Back</a>
    </form>

@endsection