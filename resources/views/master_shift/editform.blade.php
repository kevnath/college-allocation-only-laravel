@extends('master.template')

@section('title', 'Update Campus' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/update-shift">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="put" />
        <input type="hidden" name="id" value="{{ $shift->id }}" />
        <div class="form-group">
            <input type="text" placeholder="Shift Time" name="time" class="form-control" value="{{ $shift->time }}">
        </div>

        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/course" class="btn btn-default">Back</a>
    </form>

@endsection