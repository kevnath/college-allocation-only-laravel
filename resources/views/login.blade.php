<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset("assets/css/bootstrap.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("assets/css/login.css") }}">
    <script src="{{asset("assets/js/jquery-2.2.0.min.js")}}"></script>
    <script type="text/javascript" src="{{asset("assets/js/bootstrap.min.js")}}"></script>
    <script>
        $(document).ready(function() {
            $('.form-control')[0].focus();
        });
    </script>
</head>
<body>
<div class="container-fluid">
    <div class="login-section">
        <div class="login-form">
            <figure>
                <img src="{{ asset("assets/img/logo-slc.png") }}" style="width: 130px; margin-left: 20px;"/>
            </figure>
            <h3>College Allocation System</h3>
            <form method="post" action="/doLogin">
                {!! csrf_field() !!}
                <div class="form-group">
                    <input type="text" placeholder="Username" class="form-control" name="username"
                           value="{!! old('username') !!}" required/>
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" class="form-control" name="password"
                            required />
                </div>
                <input type="submit" value="Login" class="btn btn-default form-control mybtn" />
            </form>
        </div>
        @include('errors.dialogerror')
    </div>
</div>
</body>
</html>