@if($errors->any())
    <div class="alert alert-danger" style="margin: 10px 0;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! $errors->first() !!}
    </div>
@endif