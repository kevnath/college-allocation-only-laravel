@extends('master.template')

@section('title', 'Insert Course' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/insert-course">
        {!! csrf_field() !!}
        <div class="form-group">
            <input type="text" placeholder="Course ID" name="id" class="form-control" value="{{ old('id') }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Course Name" name="name" class="form-control" value="{{ old('name') }}">
        </div>
        <div class="form-group">
            <label for="alt">Or, upload with Excel</label>
            <input type="file" name="fileExcel" accept=".csv" />
        </div>
        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/course" class="btn btn-default">Back</a>
    </form>

@endsection