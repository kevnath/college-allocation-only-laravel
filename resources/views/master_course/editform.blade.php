@extends('master.template')

@section('title', 'Update Ast and Staff' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/update-course">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="put" />
        <div class="form-group">
            <input type="text" placeholder="Course ID" name="id" class="form-control" value="{{ $course->id }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Course Name" name="name" class="form-control" value="{{ $course->name }}">
        </div>
        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/course" class="btn btn-default">Back</a>
    </form>

@endsection