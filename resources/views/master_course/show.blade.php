@extends('master.template')

@section('title', 'Show Course')

@section('bagian')
    <div class="btn-container">
        <a href="/insert-course" class="btn btn-default right">Insert Data</a>
    </div>

    @include('errors.success')

    @if(count($courses) == 0)
        <div>No course found</div>
    @else
        <table class="table table-bordered table-hover table-striped table-condensed" id="table">
            <thead>
                <tr>
                    <th rowspan="2">Course ID</th>
                    <th rowspan="2">Course Name</th>
                    <th colspan="2">Action</th>
                </tr>
                <tr>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            @foreach($courses as $course)
                <tr>
                    <td>{!! $course->id !!}</td>
                    <td>{!! $course->name !!}</td>
                    <td><a href="/course/{!! $course->id !!}">Edit</a></td>
                    <td><a href="/course/{!! $course->id !!}" data-method="delete" data-token="{{ csrf_token() }}"
                           data-confirm="Are you sure?">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <script src="{{ asset('assets/js/delete.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/startDatatable.js') }}"></script>
    @endif

@endsection