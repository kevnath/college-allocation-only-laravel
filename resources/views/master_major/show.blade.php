@extends('master.template')

@section('title', 'Show Major')

@section('bagian')
    <div class="btn-container">
        <a href="/insert-major" class="btn btn-default right">Insert Major</a>
    </div>

    @include('errors.success')
    @if(count($majors) == 0)
        <div>No major found</div>
    @else
    <table class="table table-bordered table-hover table-striped table-condensed" id="table">
        <thead>
            <tr>
                <th rowspan="2">No.Code</th>
                <th rowspan="2">Code</th>
                <th rowspan="2">Alt. Code</th>
                <th rowspan="2">Name</th>
                <th colspan="2">Action</th>
            </tr>
            <tr>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
        @foreach($majors as $major)
            <tr>
                <td>{!! $major->id !!}</td>
                <td>{!! $major->code !!}</td>
                <td>{!! $major->alt_code !!}</td>
                <td>{!! $major->name !!}</td>
                <td><a href="/major/{!! $major->id !!}">Edit</a></td>
                <td><a href="/major/{!! $major->id !!}" data-method="delete" data-token="{{ csrf_token() }}"
                       data-confirm="Are you sure?">Delete</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <script src="{{ asset('assets/js/delete.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/startDatatable.js') }}"></script>
    @endif
@endsection