@extends('master.template')

@section('title', 'Insert Major')

@section('bagian')
        <form role="form" method="post" action="/insert-major" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="text" placeholder="No. Code" class="form-control" name="id" value="{{ old('id') }}" />
            </div>
            <div class="form-group">
                <input type="text" placeholder="Code" class="form-control" name="kode1" value="{{ old('kode1') }}"/>
            </div>
            <div class="form-group">
                <input type="text" placeholder="Alt. Code" class="form-control" name="kode2" value="{{ old('kode2') }}"/>
            </div>
            <div class="form-group">
                <input type="text" placeholder="Name" class="form-control" name="nama_jurusan" value="{{ old('nama_jurusan') }}"/>
            </div>
            <div class="form-group">
                <label for="alt">Or, upload with Excel</label>
                <input type="file" name="fileExcel" accept=".csv" />
            </div>
            @include('errors.dialogerror')
            <button type="submit" class="btn btn-default" id="btnSubmit">Submit Major</button>
            <a href="/master-major" class="btn btn-default">Back</a>
        </form>

@endsection