@extends('master.template')

@section('title', 'Update Major')

@section('bagian')
    <form role="form" method="post" action="/update-major">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="PUT" />
        <div class="form-group">
            <input type="text" placeholder="No. Code" class="form-control" name="id" value="{{ $major->id }}" />
        </div>
        <div class="form-group">
            <input type="text" placeholder="Code" class="form-control" name="kode1" value="{{ $major->code }}"/>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Alt. Code" class="form-control" name="kode2" value="{{ $major->alt_code }}"/>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Name" class="form-control" name="nama_jurusan" value="{{ $major->name }}"/>
        </div>
        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit Major</button>
        <a href="/major" class="btn btn-default">Back</a>
    </form>

@endsection