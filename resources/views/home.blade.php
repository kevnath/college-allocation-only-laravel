@extends('master.template')

@section('title', 'College Allocation System')

@section('bagian')
    <div class="col-md-10">
        @if($counter->locationCount == 0)
            <div class="alert alert-info">
                Location is empty. <a href="/insert-location" target="_blank">Click here to insert.</a>
            </div>
        @endif
        @if($counter->campusCount == 0)
            <div class="alert alert-info">
                Campus is empty. <a href="/insert-campus" target="_blank">Click here to insert.</a>
            </div>
        @endif
        @if($counter->majorCount == 0)
            <div class="alert alert-info">
                Major is empty. <a href="/insert-major" target="_blank">Click here to insert.</a>
            </div>
        @endif
        @if($counter->shiftCount == 0)
            <div class="alert alert-info">
                Shift is empty. <a href="/insert-shift" target="_blank">Click here to insert.</a>
            </div>
        @endif
        @if($counter->courseCount == 0)
            <div class="alert alert-info">
                Course is empty. <a href="/insert-course" target="_blank">Click here to insert.</a>
            </div>
        @endif
        @if($counter->astCount == 0)
            <div class="alert alert-info">
                Assistant is empty. <a href="/insert-ast-staff" target="_blank">Click here to insert.</a>
            </div>
        @endif
        @if($counter->altScheduleCount == 0)
            <div class="alert alert-info">
                Schedules are empty. <a href="/insert-alternate-schedule" target="_blank">Click here to insert.</a>
            </div>
        @endif
    </div>
    <div class="col-md-2">
        <h4>Welcome, {{ \Illuminate\Support\Facades\Auth::user()->username }}</h4>
        <div class="menu-right">
            <a href="downloads/template" target="_blank" class="btn btn-default">Download Excel Template</a>
        </div>
        <div class="menu-right">
            <a href="deleteAll" class="btn btn-default" id="delete">Delete All Data</a>
        </div>
        <script>
            $(document).ready(function () {
                $('#delete').click(function(e) {
                    var answer = confirm("Are you sure? This will delete all ast, courses, majors, and schedules data!");
                    if(!answer) {
                        e.preventDefault();
                    }
                });
            });
        </script>
    </div>
@endsection