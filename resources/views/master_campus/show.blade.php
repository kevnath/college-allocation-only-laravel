@extends('master.template')

@section('title', 'Show Campus')

@section('bagian')
    <div class="btn-container">
        <a href="/insert-campus" class="btn btn-default right">Insert Data</a>
    </div>

    @include('errors.success')

    @if(count($campuses) == 0)
        <div>No campus found</div>
    @else
        <table class="table table-bordered table-hover table-striped table-condensed" id="table">
            <thead>
                <tr>
                    <th rowspan="2">Campus</th>
                    <th rowspan="2">Location</th>
                    <td colspan="2">Action</td>
                </tr>
                <tr>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            @foreach($campuses as $campus)
                <tr>
                    <td>{!! $campus->name !!}</td>
                    <td>{!! $campus->getCampusLocation->name !!}</td>
                    <td><a href="/campus/{!! $campus->id !!}">Edit</a></td>
                    <td><a href="/campus/{!! $campus->id !!}" data-method="delete" data-token="{{ csrf_token() }}"
                           data-confirm="Are you sure?">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <script src="{{ asset('assets/js/delete.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/startDatatable.js') }}"></script>
    @endif

@endsection