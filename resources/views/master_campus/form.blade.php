@extends('master.template')

@section('title', 'Insert Campus' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/insert-campus">
        {!! csrf_field() !!}
        <div class="form-group">
            <input type="text" placeholder="Campus" name="name" class="form-control" value="{{ old('name') }}">
        </div>
        <div class="form-group">
            <label for="location">Location</label>
            <select name="location" class="form-control">
                @foreach($locations as $location)
                    <option value="{{ $location->id }}">{{ $location->name }}</option>
                @endforeach
            </select>
        </div>

        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/course" class="btn btn-default">Back</a>
    </form>
    <script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2/startselect2.js') }}"></script>
@endsection