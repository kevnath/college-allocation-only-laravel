@extends('master.template')

@section('title', 'Show Alternate Schedule')

@section('bagian')
    <div class="btn-container">
        <a href="/insert-alternate-schedule" class="btn btn-default right">Insert Data</a>
    </div>

    @include('errors.success')

    @if(count($altSchedule) == 0)
        <div>No Alternate Schedule Found</div>
    @else
        <table class="table table-bordered table-hover table-striped table-condensed" id="table">
            <thead>
                <tr>
                    <th rowspan="2">Day</th>
                    <th rowspan="2">Shift</th>
                    <th rowspan="2">Mid</th>
                    <th rowspan="2">Course ID</th>
                    <th rowspan="2">Course Name</th>
                    <th rowspan="2">Class</th>
                    <th rowspan="2">Room</th>
                    <th rowspan="2">Global</th>
                    <th rowspan="2">Location</th>
                    <th rowspan="2">Capacity</th>
                    <th rowspan="2">Occupied</th>
                    <th rowspan="2">KRS</th>
                    <th colspan="2">Action</th>
                </tr>
                <tr>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            @foreach($altSchedule as $alt)
                <tr>
                    <td>{!! $alt->schedule_day !!}</td>
                    <td>{!! $alt->college_shift !!}</td>
                    <td>{!! $alt->mid_code !!}</td>
                    <td>{!! $alt->course_id !!}</td>
                    <td>{!! $alt->findAllCourses->name !!}</td>
                    <td>{!! $alt->class !!}</td>
                    <td>{!! $alt->room !!}</td>
                    <td>{!! $alt->global !!}</td>
                    <td>{!! $alt->getCampusLocation->name !!}</td>
                    <td>{!! $alt->capacity !!}</td>
                    <td>{!! $alt->occupied !!}</td>
                    <td>{!! $alt->krs_status !!}</td>
                    <td><a href="/alternate-schedule/{!! $alt->schedule_key !!}">Edit</a></td>
                    <td><a href="/alternate-schedule/{!! $alt->schedule_key !!}" data-method="delete" data-token="{{ csrf_token() }}"
                           data-confirm="Are you sure?">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <script src="{{ asset('assets/js/delete.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/startDatatable.js') }}"></script>
    @endif
@endsection