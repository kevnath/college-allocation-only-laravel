@extends('master.template')

@section('title', 'Insert Alternate Schedule' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/insert-alternate-schedule">
        {!! csrf_field() !!}
        <div class="form-group">
            <input type="text" placeholder="Schedule Key" name="schedule_key" class="form-control" disabled>
        </div>
        <div class="form-group">
            <label for="course_id">Course ID</label>
            <select name="course_id" class="form-control">
                <option value="None">-- Course ID --</option>
                @foreach($courses as $course)
                    <option value="{!! $course->id !!}" {{ old('course_id') == $course->id ? 'selected' : ''  }}>
                        {!! $course->id !!} - {!! $course->name !!}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="shift">Shift</label>
            <select name="shift" class="form-control">
                <option value="0">-- Select --</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
            </select>
        </div>
        <div class="form-group">
            <label for="day">Day</label>
            <select name="day" class="form-control">
                <option value="0">-- Select --</option>
                <option value="1">Monday</option>
                <option value="2">Tuesday</option>
                <option value="3">Wednesday</option>
                <option value="4">Thursday</option>
                <option value="5">Friday</option>
                <option value="6">Saturday</option>
            </select>
        </div>
        <div class="form-group">
            <label for="mid">Mid Code</label>
            <select name="mid" class="form-control">
                <option value="0">-- Select --</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="5">5</option>
            </select>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Room" name="room" class="form-control" value="{{ old('room') }}">
        </div>
        <div class="form-group">
            <label for="global">Global</label>
            <input type="radio" class="radio-inline" name="global" value="Y" {{ old('global') == 'Y' ? 'checked' : '' }}> Yes
            <input type="radio" class="radio-inline" name="global" value="N" {{ old('global') == 'N' ? 'checked' : '' }}> No
        </div>
        <div class="form-group">
            <label for="location">Location</label>
            <select name="location" class="form-control">
                <option value="None">-- Location --</option>
                @foreach($campuses as $campus)
                    <option value="{!! $campus->id !!}" {{ old('name') == $campus->name ? 'selected' : ''  }}>
                        {!! $campus->name !!}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Capacity" name="capacity" class="form-control" value="{{ old('capacity') }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Occupied" name="occupied" class="form-control" value="{{ old('occupied') }}">
        </div>
        <div class="form-group">
            <label for="status_krs">KRS Status</label>
            <input type="radio" class="radio-inline" name="krs_status" value="Y" {{ old('krs_status') == 'Y' ? 'checked' : '' }}> Yes
            <input type="radio" class="radio-inline" name="krs_status" value="N" {{ old('krs_status') == 'N' ? 'checked' : '' }}> No
        </div>
        <div class="form-group">
            <input type="text" placeholder="Class" name="class" class="form-control" value="{{ old('class') }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Class Child 1" name="class_child_1" class="form-control" value="{{ old('classchild1') }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Class Child 2" name="class_child_2" class="form-control" value="{{ old('classchild2') }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Class Child 3" name="class_child_3" class="form-control" value="{{ old('classchild3') }}">
        </div>
        <div class="form-group">
            <label for="alt">Or, upload with Excel</label>
            <input type="file" name="fileExcel" accept=".csv" />
        </div>
        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/alternate-schedule" class="btn btn-default">Back</a>
    </form>
    <script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2/startselect2.js') }}"></script>
@endsection