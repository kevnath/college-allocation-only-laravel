@extends('master.template')

@section('title', 'Update Alternate Schedule' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/update-alternate-schedule">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="put" />
        <div class="form-group">
            <input type="text" placeholder="Schedule Key" name="schedule_key" class="form-control" value="{{ $alt->schedule_key }}">
        </div>
        <div class="form-group">
            <label for="course_id">Course ID</label>
            <select name="course_id" class="form-control">
                <option value="None">-- Course ID --</option>
                @foreach($courses as $course)
                    <option value="{!! $course->id !!}" {{ $alt->course_id == $course->id ? 'selected' : ''  }}>
                        {!! $course->id !!} - {!! $course->name !!}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="shift">Shift</label>
            <select name="shift" class="form-control">
                <option value="0">-- Select --</option>
                @for($i=1; $i<8; $i++)
                    <option value="{{ $i }}" {{ $alt->college_shift == $i ? 'selected' : ''  }}>
                        {{ $i }}
                    </option>
                @endfor
            </select>
        </div>
        <div class="form-group">
            <label for="day">Day</label>
            <select name="day" class="form-control">
                <option value="0">-- Select --</option>
                <option value="1" {{ $alt->schedule_day == 1 ? 'selected' : '' }}>Monday</option>
                <option value="2" {{ $alt->schedule_day == 2 ? 'selected' : '' }}>Tuesday</option>
                <option value="3" {{ $alt->schedule_day == 3 ? 'selected' : '' }}>Wednesday</option>
                <option value="4" {{ $alt->schedule_day == 4 ? 'selected' : '' }}>Thursday</option>
                <option value="5" {{ $alt->schedule_day == 5 ? 'selected' : '' }}>Friday</option>
                <option value="6" {{ $alt->schedule_day == 6 ? 'selected' : '' }}>Saturday</option>
            </select>
        </div>
        <div class="form-group">
            <label for="mid">Mid Code</label>
            <select name="mid" class="form-control">
                <option value="0">-- Select --</option>
                <option value="1" {{ $alt->mid_code == 1 ? 'selected' : '' }}>1</option>
                <option value="2" {{ $alt->mid_code == 2 ? 'selected' : '' }}>2</option>
                <option value="5" {{ $alt->mid_code == 5 ? 'selected' : '' }}>5</option>
            </select>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Room" name="room" class="form-control" value="{{ $alt->room }}">
        </div>
        <div class="form-group">
            <label for="global">Global</label>
            <input type="radio" class="radio-inline" name="global" value="Y" {{ $alt->global == 'Y' ? 'checked' : '' }}> Yes
            <input type="radio" class="radio-inline" name="global" value="N" {{ $alt->global == 'N' ? 'checked' : '' }}> No
        </div>
        <div class="form-group">
            <label for="location">Location</label>
            <select name="location" class="form-control">
                <option value="None">-- Location --</option>
                @foreach($campuses as $campus)
                    <option value="{!! $campus->id !!}}" {{ $alt->campus_id == $campus->id ? 'selected' : ''  }}>
                        {!! $campus->name !!}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Capacity" name="capacity" class="form-control" value="{{ $alt->capacity }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Occupied" name="occupied" class="form-control" value="{{ $alt->occupied }}">
        </div>
        <div class="form-group">
            <label for="status_krs">KRS Status</label>
            <input type="radio" class="radio-inline" name="krs_status" value="Y" {{ $alt->krs_status == 'Y' ? 'checked' : '' }}> Yes
            <input type="radio" class="radio-inline" name="krs_status" value="N" {{ $alt->krs_status == 'N' ? 'checked' : '' }}> No
        </div>
        <div class="form-group">
            <input type="text" placeholder="Class" name="class" class="form-control" value="{{ $parentClass }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Class Child 1" name="class_child_1" class="form-control" value="{{ $alt->class_child_1 }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Class Child 2" name="class_child_2" class="form-control" value="{{ $alt->class_child_2 }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Class Child 3" name="class_child_3" class="form-control" value="{{ $alt->class_child_3 }}">
        </div>
        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/alternate-schedule" class="btn btn-default">Back</a>
    </form>
    <script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2/startselect2.js') }}"></script>
@endsection