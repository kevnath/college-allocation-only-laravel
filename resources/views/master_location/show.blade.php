@extends('master.template')

@section('title', 'Show Location')

@section('bagian')
    <div class="btn-container">
        <a href="/insert-location" class="btn btn-default right">Insert Data</a>
    </div>

    @include('errors.success')

    @if(count($locations) == 0)
        <div>No location found</div>
    @else
        <table class="table table-bordered table-hover table-striped table-condensed" id="table">
            <thead>
                <tr>
                    <th rowspan="2">Location</th>
                    <th colspan="2">Action</th>
                </tr>
                <tr>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            @foreach($locations as $location)
                <tr>
                    <td>{!! $location->name !!}</td>
                    <td><a href="/location/{!! $location->id !!}">Edit</a></td>
                    <td><a href="/location/{!! $location->id !!}" data-method="delete" data-token="{{ csrf_token() }}"
                           data-confirm="Are you sure?">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <script src="{{ asset('assets/js/delete.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/startDatatable.js') }}"></script>
    @endif

@endsection