@extends('master.template')

@section('title', 'Insert Location' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/insert-location">
        {!! csrf_field() !!}
        <div class="form-group">
            <input type="text" placeholder="Location" name="name" class="form-control" value="{{ old('name') }}">
        </div>

        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/course" class="btn btn-default">Back</a>
    </form>

@endsection