@extends('master.template')

@section('title', 'Update Location' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/update-location">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="put" />
        <input type="hidden" name="id" value="{{ $location->id }}" />
        <div class="form-group">
            <input type="text" placeholder="Location" name="name" class="form-control" value="{{ $location->name }}">
        </div>

        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/course" class="btn btn-default">Back</a>
    </form>

@endsection