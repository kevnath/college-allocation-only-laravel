@extends('master.template')

@section('title', 'Insert Lecture Schedule' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/insert-lecture">
        {!! csrf_field() !!}
        <div class="form-group">
            <label for="initial">Initial</label>
            <select name="initial" class="form-control">
                <option value="0">-- Initial --</option>
                @foreach($initials as $initial)
                    <option value="{{ $initial->initial }}" {{ old('initial') == $initial->initial ? 'selected':'' }}>{{ $initial->initial }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="initial">Course</label>
            <select name="course" class="form-control" id="course">
                <option value="0">-- Course --</option>
                @foreach($courses as $course)
                    <option value="{{ $course->id }}" {{ old('course') == $course->id ? 'selected':'' }}>{{ $course->id }} - {{ $course->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="class">Class</label>
            <select name="class" class="form-control" id="class">
                <option value="0" selected>-- Class --</option>
            </select>
        </div>
        <div class="form-group">
            <label for="day">Day</label>
            <select name="day" class="form-control" id="day">
                <option value="0">-- Day --</option>
            </select>
        </div>
        <div class="form-group">
            <label for="shift">Shift</label>
            <select name="shift" class="form-control" id="shift">
                <option value="0">-- Shift --</option>
            </select>
        </div>
        <div class="form-group">
            <label for="alt">Or, upload with Excel</label>
            <input type="file" name="fileExcel" accept=".csv" />
        </div>
        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/lecture" class="btn btn-default">Back</a>
    </form>

<<<<<<< HEAD
=======
    <script src="{{ asset('assets/js/selectlecture.js') }}"></script>
    <script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2/startselect2.js') }}"></script>

>>>>>>> remote/mm-branch
@endsection