@extends('master.template')

@section('title', 'Update Ast and Staff' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/update-lecture">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="put" />
        <input type="hidden" name="old_schedule_key" value="{!! $lecture->schedule_key !!}" />
        <div class="form-group">
            <label for="initial">Initial</label>
            <select name="initial" class="form-control" readonly>
                <option value="0">-- Initial --</option>
                @foreach($initials as $initial)
                    <option value="{!! $initial->initial !!}" {{ $initial->initial == $lecture->initial ? 'selected' : '' }}>
                        {{ $initial->initial }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="initial">Course</label>
            <select name="course" class="form-control" id="course">
                <option value="0">-- Course --</option>
                @foreach($courses as $course)
                    <option value="{{ $course->id }}" {{ $lecture->alt_schedule->course_id == $course->id ? 'selected':'' }}>{{ $course->id }} - {{ $course->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="class">Class</label>
            <select name="class" class="form-control" id="class">
                <option value="0">-- Class --</option>
                @foreach($classes as $class)
                    <option value="{{ $class->class }}" {{ $lecture->alt_schedule->class == $class->class ? 'selected':'' }}>{{ $class->class }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="day">Day</label>
            <select name="day" class="form-control" id="day">
                <option value="0">-- Day --</option>
                @foreach($days as $day)
                    <option value="{{ $day->schedule_day }}" {{ $lecture->alt_schedule->schedule_day == $day->schedule_day ? 'selected':'' }}>{{ $day->schedule_day  }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="shift">Shift</label>
            <select name="shift" class="form-control" id="shift">
                <option value="0">-- Shift --</option>
                @foreach($shifts as $shift)
                    <option value="{{ $shift->college_shift }}" {{ $lecture->alt_schedule->college_shift == $shift->college_shift ? 'selected':'' }}>{{ $shift->college_shift }}</option>
                @endforeach
            </select>
        </div>

        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/lecture" class="btn btn-default">Back</a>
    </form>

<<<<<<< HEAD
=======
    <script src="{{ asset('assets/js/selectlecture.js') }}"></script>
    <script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2/startselect2.js') }}"></script>

>>>>>>> remote/mm-branch
@endsection