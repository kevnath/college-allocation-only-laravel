@extends('master.template')

@section('title', 'Show Lecture Schedule')

@section('bagian')
    <div class="btn-container">
        <a href="/insert-lecture" class="btn btn-default right">Insert Data</a>
    </div>

    @include('errors.success')

    @if(count($lectures) == 0)
        <div>No lecture found</div>
    @else
        <table class="table table-bordered table-hover table-striped table-condensed" id="table">
            <thead>
                <tr>
                    <th rowspan="2">Initial</th>
                    <th rowspan="2">Generation</th>
                    <th rowspan="2">NIM</th>
                    <th rowspan="2">Name</th>
                    <th rowspan="2">Major</th>
                    <th rowspan="2">Semester</th>
                    <th rowspan="2">Global</th>
                    <th rowspan="2">Course ID</th>
                    <th rowspan="2">Class</th>
                    <th rowspan="2">Day</th>
                    <th rowspan="2">Shift</th>
                    <th rowspan="2">Kode Mid</th>
                    <th rowspan="2">Room</th>
                    <th colspan="2">Action</th>
                </tr>
                <tr>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            @foreach($lectures as $lecture)
                <tr>
                    <td>{!! substr($lecture->initial,0,2) !!}</td>
                    <td>{!! substr($lecture->initial, 2) !!}</td>
                    <td>{!! $lecture->getAstSchedule->nim !!}</td>
                    <td>{!! $lecture->getAstSchedule->name !!}</td>
                    <td>{!! $lecture->getAstSchedule->getMyMajor->name !!}</td>
                    <td>{!! $lecture->getAstSchedule->semester !!}</td>
                    <td>{!! $lecture->getAstSchedule->global !!}</td>
                    <td>{!! $lecture->alt_schedule->course_id !!}</td>
                    <td>{!! $lecture->alt_schedule->class !!}</td>
                    <td>{!! $lecture->alt_schedule->schedule_day !!}</td>
                    <td>{!! $lecture->alt_schedule->college_shift !!}</td>
                    <td>{!! $lecture->alt_schedule->mid_code !!}</td>
                    <td>{!! $lecture->alt_schedule->room !!}</td>
                    <td><a href="/lecture/{!! $lecture->initial !!}/{!! $lecture->schedule_key !!}">Edit</a></td>
                    <td>
                        <a href="/lecture/{!! $lecture->initial !!}/{!! $lecture->schedule_key !!}" data-method="delete" data-token="{{ csrf_token() }}" data-confirm="Are you sure?">
                            Delete
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <script src="{{ asset('assets/js/delete.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/startDatatable.js') }}"></script>
    @endif

@endsection