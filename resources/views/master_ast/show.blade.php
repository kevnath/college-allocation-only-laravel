@extends('master.template')

@section('title', 'Show Ast and Staff')

@section('bagian')
    <div class="btn-container">
        <a href="/insert-ast-staff" class="btn btn-default right">Insert Data</a>
    </div>

    @include('errors.success')

    @if(count($allAst) == 0)
        <div>No ast found</div>
    @else
        <table class="table table-bordered table-hover table-striped table-condensed" id="table">
            <thead>
                <tr>
                    <th rowspan="2">Initial</th>
                    <th rowspan="2">Generation</th>
                    <th rowspan="2">Name</th>
                    <th rowspan="2">NIM</th>
                    <th rowspan="2">Semester</th>
                    <th rowspan="2">KRS</th>
                    <th rowspan="2">Position</th>
                    <th rowspan="2">Global</th>
                    <th rowspan="2">Major</th>
                    <th rowspan="2">Location</th>
                    <th rowspan="2">Shift</th>
                    <th rowspan="2">College</th>
                    <th colspan="2">Action</th>
                </tr>
                <tr>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            @foreach($allAst as $ast)
                <tr>
                    <td>{!! substr($ast->initial, 0, 2) !!}</td>
                    <td>{!! substr($ast->initial, 2) !!}</td>
                    <td>{!! $ast->name !!}</td>
                    <td>{!! $ast->nim !!}</td>
                    <td>{!! $ast->semester !!}</td>
                    <td>{!! $ast->krs_status!!}</td>
                    <td>{!! $ast->position !!}</td>
                    <td>{!! $ast->global !!}</td>
                    <td>{!! $ast->getMyMajor->name !!}</td>
                    <td>{!! $ast->getMyLocation->name !!}</td>
                    <td>{!! $ast->getMyShift->time !!}</td>
                    <td>{!! $ast->college_status !!}</td>
                    <td><a href="/ast-staff/{!! $ast->initial !!}">Edit</a></td>
                    <td>
                        <a href="/ast-staff/{{ $ast->initial }}"
                           data-method="delete" data-token="{{ csrf_token() }}"
                           data-confirm="Are you sure?">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <script src="{{ asset('assets/js/delete.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/datatable/startDatatable.js') }}"></script>
    @endif

@endsection