@extends('master.template')

@section('title', 'Update Ast and Staff' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/update-ast-staff">
        {!! csrf_field() !!}
        <input type="hidden" name="_method" value="put" />
        <div class="form-group">
            <input type="text" placeholder="Initial" name="initial" class="form-control" value="{{ substr($ast->initial, 0, 2) }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Generation" name="gen" class="form-control" value="{{ substr($ast->initial, 2, 4) }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="NIM" name="nim" class="form-control" value="{{ $ast->nim  }}">
        </div>
        <div class="form-group">
            <input type="number" placeholder="Semester" name="semester" class="form-control" value="{{ $ast->semester }}">
        </div>
        <div class="form-group">
            <label for="status_krs">KRS Status</label>
            <input type="radio" class="radio-inline" name="krs_status" value="Y" {{ $ast->krs_status == 'Y' ? 'checked' : '' }}> Yes
            <input type="radio" class="radio-inline" name="krs_status" value="N" {{ $ast->krs_status == 'N' ? 'checked' : '' }}> No
        </div>
        <div class="form-group">
            <label for="status_college">College Status</label>
            <input type="radio" class="radio-inline" name="college_status" value="Y" {{ $ast->college_status == 'Y' ? 'checked' : '' }}> Yes
            <input type="radio" class="radio-inline" name="college_status" value="N" {{ $ast->college_status == 'N' ? 'checked' : '' }}> No
        </div>
        <div class="form-group">
            <label for="major">Major</label>
            <select name="major" class="form-control">
                <option value="0">-- Major --</option>
                @foreach($majors as $major)
                    <option value="{!! $major->id !!}" {{ $ast->major_id == $major->id ? 'selected' : ''  }}>
                        {!! $major->name !!}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="position">Ast / Staff</label>
            <input type="radio" class="radio-inline" name="position" value="Ast" {{ $ast->position == 'Ast' ? 'checked' : '' }}> Assistant
            <input type="radio" class="radio-inline" name="position" value="Staff" {{ $ast->position == 'Staff' ? 'checked' : '' }}> Staff
        </div>
        <div class="form-group">
            <label for="location">Location</label>
            <select name="location" class="form-control">
                <option value="0">-- Location --</option>
                @foreach($locations as $location)
                    <option value="{!! $location->id !!}" {{ $ast->location_id == $location->id ? 'selected' : ''  }}>
                        {!! $location->name !!}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="global">Global</label>
            <input type="radio" class="radio-inline" name="global" value="Y" {{ $ast->global == 'Y' ? 'checked' : '' }}> Yes
            <input type="radio" class="radio-inline" name="global" value="N" {{ $ast->global == 'N' ? 'checked' : '' }}> No
        </div>
        <div class="form-group">
            <label for="shift">Shift</label>
            @foreach($shifts as $shift)
                <input type="radio" class="radio-inline" name="shift" value="{!! $shift->id !!}"
                        {{ $ast->shift_id == $shift->id ? 'checked' : '' }}> {!! $shift->time !!}
            @endforeach
        </div>

        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/ast-staff" class="btn btn-default">Back</a>
    </form>
    <script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2/startselect2.js') }}"></script>
@endsection