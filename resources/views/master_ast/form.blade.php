@extends('master.template')

@section('title', 'Insert Ast and Staff' )

@section('bagian')
    <form role="form" method="post" enctype="multipart/form-data" action="/insert-ast-staff">
        {!! csrf_field() !!}
        <div class="form-group">
            <input type="text" placeholder="Initial" name="initial" class="form-control" value="{{ strtoupper(old('initial')) }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Generation" name="gen" class="form-control" value="{{ old('gen') }}">
        </div>
        <div class="form-group">
            <input type="text" placeholder="NIM" name="nim" class="form-control" value="{{ old('nim')  }}">
        </div>
        <div class="form-group">
            <input type="number" placeholder="Semester" name="semester" class="form-control" value="{{ old('semester') }}">
        </div>
        <div class="form-group">
            <label for="status_krs">KRS Status</label>
            <input type="radio" class="radio-inline" name="krs_status" value="Y" {{ old('krs_status') == 'Y' ? 'checked' : '' }}> Yes
            <input type="radio" class="radio-inline" name="krs_status" value="N" {{ old('krs_status') == 'N' ? 'checked' : '' }}> No
        </div>
        <div class="form-group">
            <label for="status_college">College Status</label>
            <input type="radio" class="radio-inline" name="college_status" value="Y" {{ old('college_status') == 'Y' ? 'checked' : '' }}> Yes
            <input type="radio" class="radio-inline" name="college_status" value="N" {{ old('college_status') == 'N' ? 'checked' : '' }}> No
        </div>
        <div class="form-group">
            <label for="major">Major</label>
            <select name="major" class="form-control">
                <option value="0">-- Major --</option>
                @foreach($majors as $major)
                    <option value="{!! $major->id !!}" {{ old('major') == $major->id ? 'selected' : ''  }}>
                        {!! $major->name !!}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="position">Ast / Staff</label>
            <input type="radio" class="radio-inline" name="position" value="Ast" {{ old('position') == 'Ast' ? 'checked' : '' }}> Assistant
            <input type="radio" class="radio-inline" name="position" value="Staff" {{ old('position') == 'Staff' ? 'checked' : '' }}> Staff
        </div>
        <div class="form-group">
            <label for="location">Location</label>
            <select name="location" class="form-control">
                <option value="0">-- Location --</option>
                @foreach($locations as $location)
                    <option value="{!! $location->id !!}" {{ old('location') == $location->id ? 'selected' : ''  }}>
                        {!! $location->name !!}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="global">Global</label>
            <input type="radio" class="radio-inline" name="global" value="Y" {{ old('global') == 'Y' ? 'checked' : '' }}> Yes
            <input type="radio" class="radio-inline" name="global" value="N" {{ old('global') == 'N' ? 'checked' : '' }}> No
        </div>
        <div class="form-group">
            <label for="shift">Shift</label>
            @foreach($shifts as $shift)
                <input type="radio" class="radio-inline" name="shift" value="{!! $shift->id !!}"
                        {{ old('shift') == $shift->id ? 'checked' : '' }}> {!! $shift->time !!}
            @endforeach
        </div>
        <div class="form-group">
            <label for="alt">Or, upload with Excel</label>
            <input type="file" name="fileExcel" accept=".csv" />
        </div>
        @include('errors.dialogerror')
        <button type="submit" class="btn btn-default" id="btnSubmit">Submit</button>
        <a href="/ast-staff" class="btn btn-default">Back</a>
    </form>

    <script src="{{ asset('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2/startselect2.js') }}"></script>
@endsection