<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserSeeder::class);
        $this->call(ShiftSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(CampusSeeder::class);

        Model::reguard();
    }
}

class UserSeeder extends Seeder {
    public function run() {
        DB::table('users')->insert([
            'username' => 'kv15-1',
            'password' => bcrypt('123456'),
        ]);

        DB::table('users')->insert([
            'username' => 'AP15-1',
            'password' => bcrypt('testong'),
        ]);

        DB::table('users')->insert([
            'username' => 'mm15-1',
            'password' => bcrypt('mm15-1'),
        ]);
    }
}

class ShiftSeeder extends Seeder {
    public function run() {
        DB::table('shifts')->insert([
            'time' => 'Pagi',
        ]);
        DB::table('shifts')->insert([
            'time' => 'Malam',
        ]);
        DB::table('shifts')->insert([
            'time' => 'Normal',
        ]);
    }
}

class LocationSeeder extends Seeder {
    public function run() {
        DB::table('locations')->insert([
            'name' => 'Kemanggisan',
        ]);
        DB::table('locations')->insert([
            'name' => 'Alam Sutera',
        ]);
        DB::table('locations')->insert([
            'name' => 'NULL',
        ]);
    }
}

class CampusSeeder extends Seeder{
    public function run(){
        DB::table('campuses')->insert([
            'location_id' => 1,
            'name' => 'Anggrek',
        ]);
        DB::table('campuses')->insert([
            'location_id' => 1,
            'name' => 'Kijang',
        ]);
        DB::table('campuses')->insert([
            'location_id' => 1,
            'name' => 'Square',
        ]);
        DB::table('campuses')->insert([
            'location_id' => 1,
            'name' => 'Syahdan',
        ]);
        DB::table('campuses')->insert([
            'location_id' => 2,
            'name' => 'ALC',
        ]);
        DB::table('campuses')->insert([
            'location_id' => 2,
            'name' => 'ASM',
        ]);
        DB::table('campuses')->insert([
            'location_id' => 3,
            'name' => 'NULL',
        ]);
    }
}