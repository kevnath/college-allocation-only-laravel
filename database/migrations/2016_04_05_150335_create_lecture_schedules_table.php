<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLectureSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecture_schedules', function (Blueprint $table) {
            $table->string('schedule_key', 50);
            $table->char('initial', 6);

            $table->primary(['schedule_key', 'initial']);
            $table->foreign('schedule_key')->references('schedule_key')->on('alt_schedules')->onDelete('cascade');
            $table->foreign('initial')->references('initial')->on('assistants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lecture_schedules');
    }
}
