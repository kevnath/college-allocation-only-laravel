<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistants', function (Blueprint $table) {
            $table->char('initial', 6);
            $table->char('nim', 10);
            $table->string('name', 40)->nullable();
            $table->integer('semester')->unsigned();
            $table->char('krs_status', 1);
            $table->char('college_status', 1);
            $table->string('position', 5);
            $table->string('major_id', 10);
            $table->char('global', 1);
            $table->integer('location_id')->unsigned();
            $table->integer('shift_id')->unsigned();

            $table->primary('initial');
            $table->foreign('major_id')->references('id')->on('majors')->onDelete('cascade');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('shift_id')->references('id')->on('shifts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assistants');
    }
}
