<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAltSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alt_schedules', function (Blueprint $table) {
            $table->string('schedule_key', 50);
            $table->char('course_id', 8);
            $table->integer('schedule_day')->unsigned();
            $table->integer('college_shift')->unsigned();
            $table->integer('mid_code')->unsigned();
            $table->string('room', 8);
            $table->string('global', 5)->nullable();
            $table->integer('campus_id')->unsigned();
            $table->integer('capacity')->unsigned();
            $table->integer('occupied')->unsigned()->nullable();
            $table->char('krs_status', 1);
            $table->char('class', 30);
            $table->char('class_child_1', 4);
            $table->char('class_child_2', 4);
            $table->char('class_child_3', 4);
            $table->timestamps();

            $table->primary('schedule_key');
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
            $table->foreign('campus_id')->references('id')->on('campuses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alt_schedules');
    }
}
