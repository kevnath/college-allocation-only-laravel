<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Assistant extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'initial';

    public function getMyLocation(){
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function getMyMajor(){
        return $this->belongsTo('App\Major', 'major_id');
    }

    public function getMyShift(){
        return $this->belongsTo('App\Shift', 'shift_id');
    }

    public function lecture_schedules(){
        return $this->hasMany('App\LectureSchedule');
    }

    public function lecture_schedule_info(){
        return $this->belongsTo('App\LectureSchedule');
    }

    private static $rules = [
        'initial' => 'required|alpha|size:2',
        'gen' => 'required|size:4',
        'nim' => 'required|size:10',
        'semester' => 'required|numeric|min:1',
        'krs_status' => 'required',
        'college_status' => 'required',
        'position' => 'required|in:Ast,Staff',
        'major' => 'required',
        'global' => 'required',
        'location' => 'required',
        'shift' => 'required|in:1,2,3'
    ];
    public static function validate($request) {
        $validator = Validator::make($request->all(), Assistant::$rules);
        return $validator;
    }
}
