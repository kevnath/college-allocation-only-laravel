<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class LectureSchedule extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'schedule_key';

    public function findAllAstSchedules(){
        return $this->hasMany('App\Assistant', 'initial');
    }

    public function getAstSchedule(){
        return $this->belongsTo('App\Assistant', 'initial');
    }

    public function get_alt_schedule(){
        return $this->hasMany('App\AltSchedule', 'schedule_key');
    }

    public function alt_schedule(){
        return $this->belongsTo('App\AltSchedule', 'schedule_key');
    }

    private static $rules = [
        'initial'   => 'required|size:6',
        'course'    => 'required',
        'class'     => 'required',
        'day'       => 'required',
        'shift'     => 'required',
    ];
    public static function validate($request) {
        $validator = Validator::make($request->all(), LectureSchedule::$rules);
        return $validator;
    }
}
