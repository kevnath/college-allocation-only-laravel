<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Course extends Model
{
    public $timestamps = false;

    public function findMySchedule(){
        return $this->hasMany('App\AltSchedule', 'course_id');
    }

    public static function getCourseNoCode($code) {
        return Course::where('id', $code)->first()->id;
    }

    private static $rules = [
        'id' => 'required',
        'name' => 'required'
    ];
    public static function validate($request) {
        $validator = Validator::make($request->all(), Course::$rules);
        return $validator;
    }

}
