<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Campus extends Model
{
    public function getCampusLocation() {
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function getAltScheduleCampusLocation(){
        return $this->hasOne('App\AltSchedule', 'campus_id');
    }

    public static function validate($request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'location' => 'required|numeric'
        ]);
        return $validator;
    }
}
