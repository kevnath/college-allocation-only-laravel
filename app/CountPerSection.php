<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountPerSection extends Model
{
    protected $attributes = [
        'astCount',
        'shiftCount',
        'campusCount',
        'locationCount',
        'courseCount',
        'majorCount',
        'altScheduleCount'
    ];
    
}
