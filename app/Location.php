<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Location extends Model
{
    public $timestamps = false;

    public static function findLocationId($request) {
        if(trim($request->location) == 'Kmg')
            return Location::where('name', 'Kemanggisan')->first()->id;
        else
            return Location::where('name', 'Alam Sutera')->first()->id;
    }

    public static function validate($request) {
        $validator = Validator::make($request->all(), ['name' => 'required']);
        return $validator;
    }

    public function getAllAstLocation() {
        return $this->hasMany('App\Assistant', 'location_id');
    }

    public function getAllCampuses() {
        return $this->hasMany('App\Campus', 'location_id');
    }
}
