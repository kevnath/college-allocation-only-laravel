<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class AltSchedule extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'schedule_key';

    public function findAllShifts(){
        return $this->belongsTo('App\Shift', 'shift_id');
    }

    public function findAllCourses(){
        return $this->belongsTo('App\Course', 'course_id');
    }

    public function lectureSchedule(){
        return $this->belongsTo('App\LectureSchedule');
    }

    public function lectureSchedules(){
        return $this->hasMany('App\LectureSchedule');
    }

    public function getCampusLocation(){
        return $this->belongsTo('App\Campus', 'campus_id');
    }

    private static $rules =[
        'course_id' => 'required',
        'shift' => 'required|min:1|max:7|numeric',
        'day' => 'required|min:1|max:6|numeric',
        'mid' => 'required|in:1,2,5|numeric',
        'room' => 'required',
        'global' => 'required',
        'location' => 'required',
        'capacity' => 'required|min:0|numeric',
        'occupied' => 'required|numeric',
        'krs_status' => 'required',
        'class' => 'required'
    ];

    public static function validate($request){
        $validator = Validator::make($request->all(), AltSchedule::$rules);
        return $validator;
    }
}
