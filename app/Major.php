<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Major extends Model
{
    public $timestamps = false;

    public function findAstMajor() {
        return $this->hasMany('App\Assistant', 'major_id');
    }

    public static function getMajorNoCode($code) {
        return Major::where('alt_code', $code)->orWhere('code', $code)->first()->id;
    }

    // validation methods
    public static $rules = [
        'id' => 'required|size:2',
        'kode1' => 'required|max:10',
        'kode2' => 'max:10',
        'nama_jurusan' => 'required|max:60'
    ];
    public static function validate($request) {
        $validator = Validator::make($request->all(), Major::$rules);
        return $validator;
    }
}
