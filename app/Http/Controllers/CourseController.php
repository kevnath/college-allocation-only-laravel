<?php

namespace App\Http\Controllers;

use App\Assistant;
use App\Course;
use App\Location;
use App\Major;
use App\Shift;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends Controller
{
    public function showAll() {
        $courses = Course::get();
        return view('master_course.show', ['courses' => $courses]);
    }

    public function showInsertForm() {
        return view('master_course.form');
    }

    public function insertData(Request $request) {
        if($request->fileExcel != null) {
            // upload via excel
            $this->insertViaExcel($request);
            return redirect('/course')->with('status', 'Uploaded successfully!');
        }

        // manual input
        $valid = Course::validate($request);
        if($valid->fails()) {
            return back()->withErrors($valid->errors())->withInput();
        }
        $course = new Course();
        $this->saveData($course, $request);
        return redirect('/course')->with('status', 'Inserted successfully!');
    }

    private function insertViaExcel($request) {
        $sheet = Excel::selectSheetsByIndex(0)->load($request->fileExcel)->get();

        foreach($sheet as $key => $row) {
            $course = Course::find($row->kode_mtk);
            if($course == null) {
                $course = new Course();
                $course->id = $row->kode_mtk;
            }
            $course->name = $row->mata_kuliah;
            $course->save();
        }
    }

    private function saveData ($course, $request) {
        $course->id = $request->id;
        $course->name = $request->name;
        $course->save();
    }

    public function showEditForm($id) {
        $course = Course::find($id);
        return view('master_course.editform', ['course'=>$course]);
    }

    public function editData(Request $request) {
        $course = Course::find($request->id);
        $this->saveData($course, $request);
        return redirect('/course')->with('status', 'Updated successfully!');
    }

    public function deleteCourse($id) {
        $course = Course::find($id);
        if($course == null)
            return back();

        $course->delete();
        return back();
    }
}
