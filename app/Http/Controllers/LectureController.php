<?php

namespace App\Http\Controllers;

use App\AltSchedule;
use App\Assistant;
use App\Course;
use App\LectureSchedule;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class LectureController extends Controller
{
    public function showAll() {
        $lectures = LectureSchedule::with('getAstSchedule', 'alt_schedule')->orderBy('initial')->get();
        return view('master_lecture.show', ['lectures' => $lectures]);
    }

    public function showInsertForm() {
        $initial = Assistant::get();
        $course = Course::get();
        return view('master_lecture.form', ['initials' => $initial, 'courses' => $course]);
    }

    public function insert($request){
        $lecture = new LectureSchedule();
        $lecture->schedule_key = $this->requestScheduleKey($request);
        $lecture->initial = trim($request->initial);
        $lecture->save();
    }

    private function insertViaExcel($request) {
        //http://www.maatwebsite.nl/laravel-excel/docs/import
        //load file excel, just load the first sheet
        $sheet = Excel::selectSheetsByIndex(0)->load($request->fileExcel)->get();

        // get value from all rowlecture
        foreach($sheet as $index => $row) {
            $initial = $row->initial.$row->gen;
            $lecture = LectureSchedule::where('schedule_key', $row->keymtk)->where('initial', $initial)->first();
            if($lecture == null) {
                $lecture = new LectureSchedule();
                $lecture->schedule_key = trim($row->keymtk);
                $lecture->initial = $initial;
                $lecture->save();
            }
        }
    }

    public function insertData(Request $request) {
        if($request->fileExcel != null) {
            $this->insertViaExcel($request);
            return redirect('/lecture')->with('status', 'Uploaded successfully!');
        }

        $valid = LectureSchedule::validate($request);
        if($valid->fails()) {
            return back()->withErrors($valid->errors())->withInput();
        }
        $findlecture = LectureSchedule::where('schedule_key', $this->requestScheduleKey($request))->where('initial', $request->initial)->first();
        if($findlecture != null){
            return back()->withErrors('Duplicate ID')->withInput();
        }
        $this->insert($request);
        return redirect('/lecture')->with('status', 'Inserted successfully!');
    }

    public function showEditForm($ast, $id) {
        $initial = Assistant::get();
        $course = Course::get();
        $lecture = LectureSchedule::where('schedule_key', $id)->where('initial', $ast)->with('alt_schedule')->first();
        $class = AltSchedule::where('course_id', $lecture->alt_schedule->course_id)->get();
        $day = AltSchedule::where('class', $lecture->alt_schedule->class)->get();
        $shift = AltSchedule::where('class', $lecture->alt_schedule->class)->where('schedule_day', $lecture->alt_schedule->schedule_day)->get();
        return view('master_lecture.editform', ['lecture' => $lecture, 'initials' => $initial, 'courses'=>$course, 'classes'=>$class, 'days'=>$day, 'shifts'=>$shift]);
    }

    public function editData(Request $request) {
        $valid = LectureSchedule::validate($request);
        if($valid->fails()) {
            return back()->withErrors($valid->errors())->withInput();
        }

        $lecture = LectureSchedule::where('schedule_key', $request->old_schedule_key)->where('initial', $request->initial)->first();
        $lecture->schedule_key = trim($request->schedule_key);
       // dd($lecture);
        $lecture->save();
        return redirect('/lecture')->with('status', 'Updated successfully!');
    }
    public function deleteLectureSchedule($ast, $id){
        $lecture = LectureSchedule::where('schedule_key', $id)->where('initial', $ast)->first();
        if($lecture->null)
            return back();

        $lecture->delete();
        return redirect('/lecture')->with('status', 'Delete Successfully!!');
    }

    //helper
    public function requestScheduleKey($request){
        return $request->class.$request->day.$request->shift;
    }

    //AJAX Insert lecture schedule
    public function getAjaxClass(Request $request){
        $class = AltSchedule::where('course_id', $request->course)->get();
        return json_encode($class);
    }
    public function getAjaxDay(Request $request){
        $day = AltSchedule::where('class', $request->class)->get();
        return json_encode($day);
    }
    public function getAjaxShift(Request $request){
        $shift = AltSchedule::where('class', $request->class)->where('schedule_day', $request->days)->get();
        return json_encode($shift);
    }
}
