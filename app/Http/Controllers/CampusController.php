<?php

namespace App\Http\Controllers;

use App\Campus;
use App\Location;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CampusController extends Controller
{
    public function getAllCampuses() {
        $campuses = Campus::all();
        return view('master_campus.show', ['campuses' => $campuses]);
    }

    public function showInsertForm() {
        $locations = Location::all();
        return view('master_campus.form', ['locations' => $locations]);
    }

    public function insertData(Request $request) {
        $validator = Campus::validate($request);
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $campus = new Campus();
        $campus->name = $request->name;
        $campus->location_id = $request->location;
        $campus->save();
        return redirect('/campus')->with('status', 'Inserted successfully');
    }

    public function showEditForm($id) {
        $campus = Campus::find($id);
        $locations = Location::all();
        return view('master_campus.editform', ['locations' => $locations, 'campus' => $campus]);
    }

    public function editData(Request $request) {
        $validator = Campus::validate($request);
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $campus = Campus::find($request->id);
        $campus->name = $request->name;
        $campus->location_id = $request->location;
        $campus->save();
        return redirect('/campus')->with('status', 'Updated successfully');
    }

    public function deleteCampus($id) {
        $campus = Campus::find($id);
        if($campus == null) {
            return back();
        }

        $campus->delete();
        return redirect('/campus')->with('status', 'Deleted successfully');
    }
}
