<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    public function getAllLocations() {
        $locations = Location::all();
        return view('master_location.show', ['locations' => $locations]);
    }

    public function showInsertForm() {
        return view('master_location.form');
    }

    public function insertData(Request $request) {
        $validator = Location::validate($request);
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $location = new Location();
        $location->name = $request->name;
        $location->save();
        return redirect('/location')->with('status', 'Inserted successfully');
    }

    public function showEditForm($id) {
        $location = Location::find($id);
        return view('master_location.editform', ['location' => $location]);
    }

    public function editData(Request $request) {
        $validator = Location::validate($request);
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $location = Location::find($request->id);
        $location->name = $request->name;
        $location->save();
        return redirect('/location')->with('status', 'Updated successfully');
    }

    public function deleteLocation($id) {
        $location = Location::find($id);
        if($location == null) {
            return back();
        }

        $location->delete();
        return redirect('/location')->with('status', 'Deleted successfully');
    }
}
