<?php

namespace App\Http\Controllers;

use App\AltSchedule;
use App\Assistant;
use App\LectureSchedule;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SchedulingController extends Controller
{
    public function schedule() {
        $initial = Assistant::find('KV15-1');
        $schedules = LectureSchedule::where('initial', 'KV15-1')->with('alt_schedule')->get();

        echo $initial->initial.'<br />';
        echo 'hari - course id - course name - shift - krs - global - location - capacity - occupied <br/>';
        foreach($schedules as $schedule) {
            $a = $schedule->alt_schedule;
            echo $a->schedule_day.' '.$a->course_id.' '.$a->findAllCourses->name.' '.$a->college_shift.' '.$a->krs_status.' '.$a->global.' '.$a->location.' '.$a->capacity.' '.$a->occupied.'<br/>';
            echo '------------------------------------------------<br/>';
            $alts = AltSchedule::where('krs_status', $a->krs_status)->where('course_id', $a->course_id)->where('location', $a->location)->whereIn('college_shift', array(5,6))->get();
            if(count($alts) == 0)
                echo "No jadwal<br/>";
            foreach($alts as $alt) {
                echo $alt->schedule_day.' '.$alt->course_id.' '.$a->findAllCourses->name.' '.$alt->college_shift.' '.$alt->krs_status.' '.$alt->global.' '.$alt->location.' '.$alt->capacity.' '.$alt->occupied.'<br/>';
            }
            echo '<br/><br/>';
        }
    }
}
