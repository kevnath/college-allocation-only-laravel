<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showLoginForm() {
        return view('login');
    }
    public function doLogout() {
        Auth::logout();
        return redirect()->guest('/login');
    }

    public function doLogin(Request $request) {
        $valid = User::validate($request);
        if($valid->fails()) {
            return back()->withErrors($valid)->withInput();
        }

        if(Auth::attempt([
            'username' => trim($request['username']),
            'password' => trim($request['password'])
        ])) {
            return redirect('/');
        }
        return back()->withInput();
    }
}
