<?php

namespace App\Http\Controllers;

use App\Major;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class MajorController extends Controller
{
    public function showAll() {
        $majors = Major::orderBy('name')->get();
        return view('master_major.show', ['majors' => $majors]);
    }

    public function showInsertForm() {
        return view('master_major.form');
    }

    private function insert($request) {
        $major = Major::find($request->id);
        if($major != null) {
            return back()->withErrors('Duplicate ID')->withInput();
        }

        $major = new Major();
        $major->id = trim($request->id);
        $major->code = trim($request->kode1);
        $major->alt_code = trim($request->kode2);
        $major->name = trim($request->nama_jurusan);
        $major->save();
    }

    private function insertViaExcel($request) {
        //http://www.maatwebsite.nl/laravel-excel/docs/import
        //load file excel, just load the first sheet
        $sheet = Excel::selectSheetsByIndex(0)->load($request->fileExcel)->get();

        // get value from all row
        foreach($sheet as $index => $row) {
            $major = Major::find($row->no_kode);
            if($major == null) {
                $major = new Major();
                $major->id = trim($row->no_kode);
            }
            $major->code = trim($row->kode1);
            $major->alt_code = trim($row->kode2);
            $major->name = trim($row->nama_jurusan);
            $major->save();
        }
    }

    public function insertMajor(Request $request) {
//		dd($request->fileExcel->getPathName());
        if($request->fileExcel != null) {
            $this->insertViaExcel($request);
            return redirect('/major')->with('status', 'Uploaded successfully!');
        }

        $valid = Major::validate($request);
        if($valid->fails()) {
            return back()->withErrors($valid->errors())->withInput();
        }
        $this->insert($request);
        return redirect('/major')->with('status', 'Inserted successfully!');
    }

    public function showEditForm($id) {
        $major = Major::find($id);
        return view('master_major.editform', ['major' => $major]);
    }

    public function editData(Request $request) {
        $valid = Major::validate($request);
        if($valid->fails()) {
            return back()->withErrors($valid->errors())->withInput();
        }

        $major = Major::find($request->id);
        $major->id = trim($request->id);
        $major->code = trim($request->kode1);
        $major->alt_code = trim($request->kode2);
        $major->name = trim($request->nama_jurusan);
        $major->save();
        return redirect('/major')->with('status', 'Updated successfully!');
    }

    public function deleteMajor($id) {
        $major = Major::find($id);
        if($major == null)
            return back();

        $major->delete();
        return back();
    }
}
