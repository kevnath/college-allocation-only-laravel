<?php

namespace App\Http\Controllers;

use App\AltSchedule;
use App\Assistant;
use App\Campus;
use App\CountPerSection;
use App\Course;
use App\Location;
use App\Major;
use App\Shift;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function home() {
        $counter = new CountPerSection();
        $counter->astCount = Assistant::count();
        $counter->shiftCount = Shift::count();
        $counter->campusCount = Campus::count();
        $counter->locationCount = Location::count();
        $counter->courseCount = Course::count();
        $counter->majorCount = Major::count();
        $counter->altScheduleCount = AltSchedule::count();

        return view('home', ['counter' => $counter]);
    }

    public function downloadTemplate() {
        // kalau error, uncomment extension=php_fileinfo.dll di php.ini
        $path = public_path().'/download/Template.xlsx';
        $title = 'Template.xlsx';
        $header = array('Content-type: application/vnd.ms-excel');
        return response()->download($path, $title, $header);
    }

    public function deleteAll() {
        DB::table('lecture_schedules')->delete();
        DB::table('alt_schedules')->delete();
        DB::table('assistants')->delete();
        DB::table('courses')->delete();
        DB::table('majors')->delete();
//        DB::table('shifts')->delete();
//        DB::table('campuses')->delete();
//        DB::table('locations')->delete();
        return back();
    }
}
