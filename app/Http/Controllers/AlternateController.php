<?php

namespace App\Http\Controllers;

use App\Campus;
use Illuminate\Http\Request;

use App\AltSchedule;
use App\Location;
use App\Shift;
use App\Major;
use App\Course;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class AlternateController extends Controller
{
    public function showAll(){
        $altSchedule = AltSchedule::with('findAllShifts', 'findAllCourses')->get();
        return view('alternate_schedule.show', ['altSchedule' => $altSchedule]);
    }

    public function showInsertForm(){
        $locations = Location::all();
        $shifts = Shift::all();
        $campus = Campus::all();
        $course = Course::all();
        return view('alternate_schedule.form', [
            'locations' => $locations,
            'campuses' => $campus,
            'courses' => $course,
            'shifts' => $shifts
        ]);
    }

    public function insertData(Request $request){
        //upload excel
        if($request->fileExcel != null) {
            // upload via excel
            $this->insertViaExcel($request);
            return redirect('/alternate-schedule')->with('status', 'Uploaded successfully!');
        }

        //manual input
        $valid = AltSchedule::validate($request);
        if($valid->fails()) {
            return back()->withErrors($valid->errors())->withInput();
        }
        if($request->occupied > $request->capacity){
            return back()->with('status', 'Occupied must be lower than capacity');
        }

        $key = $this->setScheduleKey($request);
        $alternate = AltSchedule::find($key);
        if($alternate != null) {
            return back()->withErrors('Duplicate initial');
        }

        $alt = new AltSchedule();
        $alt->schedule_key = $key;
        $this->saveData($alt, $request);
        return redirect('/alternate-schedule')->with('status', 'Inserted successfully!');
    }

    private function insertViaExcel($request){
        $sheet = Excel::selectSheetsByIndex(0)->load($request->fileExcel)->get();
        set_time_limit(300);

        foreach($sheet as $key => $row) {
            $alt = new AltSchedule();
<<<<<<< HEAD
            $alt->schedule_key = $row->key2;
=======
            $alt->schedule_key = $row->schedule_key;
>>>>>>> remote/mm-branch
            $alt->course_id = $row->kode_mtk;
            $alt->schedule_day = $row->hari;
            $alt->college_shift = $row->waktu;
            $alt->mid_code = $row->kode_mid;
            $alt->room = $row->ruang;
            $alt->global = substr($row->global, 0, 1);
            $alt->campus_id = Campus::where('name', 'like', $row->lokasi)->first()->id;
            $alt->capacity = $row->kap;
            $alt->occupied = $row->terisi;
            $alt->krs_status = substr($row->krs, 0, 1);
            $alt->class = $row->kelas;

            $splitedKey = explode("-", $row->schedule_key);
            $child1 = "";
            $child2 = "";
            $child3 = "";

            if(strlen($splitedKey[1])-2 == 8){
                $child1 = substr($splitedKey[1], 4, 4);
            }
            else if(strlen($splitedKey[1])-2 == 12){
                $child1 = substr($splitedKey[1], 4, 4);
                $child2 = substr($splitedKey[1], 8, 4);
            }
            else if(strlen($splitedKey[1]-2 == 16)){
                $child1 = substr($splitedKey[1], 4, 4);
                $child2 = substr($splitedKey[1], 8, 4);
                $child3 = substr($splitedKey[1], 12, 4);
            }

            $alt->class_child_1 = $child1;
            $alt->class_child_2 = $child2;
            $alt->class_child_3 = $child3;

            if(!empty($alt->schedule_key))
                $alt->save();
        }
    }

    private function saveData ($alt, $request) {
        $alt->course_id = $request->course_id;
        $alt->college_shift = $request->shift;
        $alt->schedule_day = $request->day;
        $alt->mid_code = $request->mid;
        $alt->room = $request->room;
        $alt->global = $request->global;
        $alt->campus_id = $request->location;
        $alt->capacity = $request->capacity;
        $alt->occupied = $request->occupied;
        $alt->krs_status = $request->krs_status;
        $alt->class = $request->course_id.'-'.$request->class.$request->class_child_1.$request->class_child_2.$request->class_child_3;
        $alt->class_child_1 = $request->class_child_1;
        $alt->class_child_2 = $request->class_child_2;
        $alt->class_child_3 = $request->class_child_3;
        $alt->save();
    }

    public function showEditForm($id) {
        $alt = AltSchedule::find($id);
        $courses = Course::all();
        $location = Location::all();
        $campus = Campus::all();
        $course = Course::all();

        //Pecahin schedule key buat dapetin kelas anak
        $splitedKey = explode("-", $alt->schedule_key);
        $child1 = "";
        $child2 = "";
        $child3 = "";

        if(strlen($splitedKey[1])-2 == 8){
            $child1 = substr($splitedKey[1], 4, 4);
        }
        else if(strlen($splitedKey[1])-2 == 12){
            $child1 = substr($splitedKey[1], 4, 4);
            $child2 = substr($splitedKey[1], 8, 4);
        }
        else if(strlen($splitedKey[1]-2 == 16)){
            $child1 = substr($splitedKey[1], 4, 4);
            $child2 = substr($splitedKey[1], 8, 4);
            $child3 = substr($splitedKey[1], 12, 4);
        }

        $splitedClass = explode("-", $alt->class);
        $parentClass = substr($splitedClass[1], 0, 4);

        return view('alternate_schedule.editform', [
            'alt' => $alt,
            'course' => $courses,
            'locations' => $location,
            'campuses' => $campus,
            'courses' => $course,
            'parentClass' => $parentClass,
            'classchild1' => $child1,
            'classchild2' => $child2,
            'classchild3' => $child3
        ]);
    }

    public function editData(Request $request) {
        $alt = AltSchedule::find($request->schedule_key);
        $valid = AltSchedule::validate($request);

        $alt->schedule_key = $this->setScheduleKey($request);

        if($valid->fails()) {
            return back()->withErrors($valid->errors())->withInput();
        }
        if($request->occupied > $request->capacity){
            return back()->with('status','Occupied must be lower than capacity');
        }
        $this->saveData($alt, $request);
        return redirect('/alternate-schedule')->with('status', 'Updated successfully!');
    }

    public function deleteSchedule($id) {
        $schedule = AltSchedule::find($id);
        if($schedule == null)
            return back();

        $schedule->delete();
        return back();
    }

    //HELPER
    private function setScheduleKey($request){
        return $request->course_id.'-'.$request->class.$request->class_child_1.$request->class_child_2.$request->class_child_3.$request->day.$request->shift;
    }
}
