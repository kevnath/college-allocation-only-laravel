<?php

namespace App\Http\Controllers;

use App\Shift;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShiftController extends Controller
{
    public function getAllShifts() {
        $shifts = Shift::all();
        return view('master_shift.show', ['shifts' => $shifts]);
    }

    public function showInsertForm() {
        return view('master_shift.form');
    }

    public function insertData(Request $request) {
        $validator = Shift::validate($request);
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $shift = new Shift();
        $shift->time = $request->time;
        $shift->save();
        return redirect('/shift')->with('status', 'Inserted successfully');
    }

    public function showEditForm($id) {
        $shift = Shift::find($id);
        return view('master_shift.editform', ['shift' => $shift]);
    }

    public function editData(Request $request) {
        $validator = Shift::validate($request);
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $shift = Shift::find($request->id);
        $shift->time = $request->time;
        $shift->save();
        return redirect('/shift')->with('status', 'Updated successfully');
    }

    public function deleteShift($id) {
        $shift = Shift::find($id);
        if($shift == null) {
            return back();
        }

        $shift->delete();
        return redirect('/shift')->with('status', 'Deleted successfully');
    }
}
