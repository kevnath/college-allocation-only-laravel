<?php

namespace App\Http\Controllers;

use App\Assistant;
use App\Location;
use App\Major;
use App\Shift;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class AssistantController extends Controller
{

    public function getAllAst() {
        $allAst = Assistant::with('getMyMajor', 'getMyLocation', 'getMyShift')->get();
        return view('master_ast.show', ['allAst' => $allAst]);
    }

    public function showInsertForm() {
        $majors = Major::orderBy('name')->get();
        $locations = Location::all();
        $shifts = Shift::all();
        return view('master_ast.form', [
            'majors' => $majors,
            'locations' => $locations,
            'shifts' => $shifts
        ]);
    }

    public function insertData(Request $request) {
        if($request->fileExcel != null) {
            // upload via excel
            $this->insertViaExcel($request);
            return redirect('/ast-staff')->with('status', 'Uploaded successfully!');
        }

        // manual input
        $valid = Assistant::validate($request);
        if($valid->fails()) {
            return back()->withErrors($valid->errors())->withInput();
        }

        $initial = $this->setInitial($request);
        $ast = Assistant::find($initial);
        if($ast != null) {
            return back()->withErrors('Duplicate initial');
        }

        $ast = new Assistant();
        $ast->initial = $initial;
        $this->saveData($ast, $request);
        return redirect('/ast-staff')->with('status', 'Inserted successfully!');
    }

    private function insertViaExcel($request) {
        $sheet = Excel::selectSheetsByIndex(0)->load($request->fileExcel)->get();

        foreach($sheet as $key => $row) {
            $initial = $this->setInitial($row);

            $ast = Assistant::find($initial);
            if($ast == null) {
                $ast = new Assistant();
                $ast->initial = $initial;
            }
            $ast->nim = $row->nim;
            $ast->name = $row->nama;
            $ast->semester = $row->semester;
            $ast->krs_status = substr($row->krs, 0, 1);
            $ast->position = $row->position;
            $ast->college_status = substr($row->college_status, 0, 1);
            $ast->major_id = Major::getMajorNoCode($row->major);
            $ast->global = substr($row->global, 0, 1);
            $ast->location_id = Location::findLocationId($row);
            $ast->shift_id = Shift::findShiftId($row);

            if(!empty($ast->initial))
                $ast->save();
        }
    }

    private function saveData ($ast, $request) {
        $ast->nim = $request->nim;
        $ast->semester = $request->semester;
        $ast->krs_status = $request->krs_status;
        $ast->college_status = $request->college_status;
        $ast->position = $request->position;
        $ast->major_id = $request->major;
        $ast->global = $request->global;
        $ast->location_id = $request->location;
        $ast->shift_id = $request->shift;
        $ast->save();
    }

    public function showEditForm($id) {
        $ast = Assistant::find($id);
        $majors = Major::orderBy('name')->get();
        $locations = Location::all();
        $shifts = Shift::all();
        return view('master_ast.editform', [
            'ast' => $ast,
            'majors' => $majors,
            'locations' => $locations,
            'shifts' => $shifts
        ]);
    }

    public function editData(Request $request) {
        $initial = $this->setInitial($request);

        $ast = Assistant::find($initial);
        $this->saveData($ast, $request);
        return redirect('/ast-staff')->with('status', 'Updated successfully!');
    }

    public function deleteAst($id) {
        $initial = Assistant::find($id);
        if($initial == null)
            return back();

        $initial->delete();
        return back();
    }

    // HELPER
    private function setInitial($request) {
        return $request->initial.$request->gen;
    }

}
