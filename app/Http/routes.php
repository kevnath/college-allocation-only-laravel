<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => 'guest'], function() {
    Route::get('/', function(){ return redirect('/login'); });
    Route::get('/login', 'LoginController@showLoginForm');
    Route::post('/doLogin', 'LoginController@doLogin');
});

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'HomeController@home');
    Route::get('/logout', 'LoginController@doLogout');
    Route::get('/downloads/template', 'HomeController@downloadTemplate');
    Route::get('/deleteAll', 'HomeController@deleteAll');
    
    //location
    Route::get('/location', 'LocationController@getAllLocations');
    Route::get('/insert-location', 'LocationController@showInsertForm');
    Route::post('/insert-location', 'LocationController@insertData');
    Route::get('/location/{id}', 'LocationController@showEditForm');
    Route::put('/update-location', 'LocationController@editData');
    Route::delete('/location/{id}', 'LocationController@deleteLocation');

    //campuses
    Route::get('/campus', 'CampusController@getAllCampuses');
    Route::get('/insert-campus', 'CampusController@showInsertForm');
    Route::post('/insert-campus', 'CampusController@insertData');
    Route::get('/campus/{id}', 'CampusController@showEditForm');
    Route::put('/update-campus', 'CampusController@editData');
    Route::delete('/campus/{id}', 'CampusController@deleteCampus');

    //shift
    Route::get('/shift', 'ShiftController@getAllShifts');
    Route::get('/insert-shift', 'ShiftController@showInsertForm');
    Route::post('/insert-shift', 'ShiftController@insertData');
    Route::get('/shift/{id}', 'ShiftController@showEditForm');
    Route::put('/update-shift', 'ShiftController@editData');
    Route::delete('/shift/{id}', 'ShiftController@deleteShift');

    //master ast dan staff
    Route::get('/ast-staff', 'AssistantController@getAllAst');
    Route::get('/insert-ast-staff', 'AssistantController@showInsertForm');
    Route::post('/insert-ast-staff', 'AssistantController@insertData');
    Route::get('/ast-staff/{id}', 'AssistantController@showEditForm');
    Route::put('/update-ast-staff', 'AssistantController@editData');
    Route::delete('/ast-staff/{id}', 'AssistantController@deleteAst');

    //master major
    Route::get('/major', 'MajorController@showAll');
    Route::get('/insert-major', 'MajorController@showInsertForm');
    Route::post('/insert-major', 'MajorController@insertMajor');
    Route::get('/major/{id}', 'MajorController@showEditForm');
    Route::put('/update-major', 'MajorController@editData');
    Route::delete('/major/{id}', 'MajorController@deleteMajor');

    //alternate schedule
    Route::get('/alternate-schedule', 'AlternateController@showAll');
    Route::get('/insert-alternate-schedule', 'AlternateController@showInsertForm');
    Route::post('/insert-alternate-schedule', 'AlternateController@insertData');
    Route::get('/alternate-schedule/{id}', 'AlternateController@showEditForm');
    Route::put('/update-alternate-schedule', 'AlternateController@editData');
    Route::delete('/alternate-schedule/{id}', 'AlternateController@deleteSchedule');

    //master lecture
    Route::get('/lecture', 'LectureController@showAll');
    Route::get('/insert-lecture', 'LectureController@showInsertForm');
    Route::post('/insert-lecture', 'LectureController@insertData');
    Route::get('/lecture/{ast}/{id}', 'LectureController@showEditForm');
    Route::put('/update-lecture', 'LectureController@editData');
    Route::delete('/lecture/{ast}/{id}', 'LectureController@deleteLectureSchedule');
<<<<<<< HEAD
=======
    Route::post("/lecture/getclass", 'LectureController@getAjaxClass');
    Route::post("/lecture/getday", 'LectureController@getAjaxDay');
    Route::post("/lecture/getshift", 'LectureController@getAjaxShift');

>>>>>>> remote/mm-branch

    //master course
    Route::get('/course', 'CourseController@showAll');
    Route::get('/insert-course', 'CourseController@showInsertForm');
    Route::post('/insert-course', 'CourseController@insertData');
    Route::get('/course/{id}', 'CourseController@showEditForm');
    Route::put('/update-course', 'CourseController@editData');
    Route::delete('/course/{id}', 'CourseController@deleteCourse');
});

Route::get('/generate', 'SchedulingController@schedule');