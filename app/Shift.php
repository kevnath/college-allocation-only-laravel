<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Shift extends Model
{
    public $timestamps = false;

    public static function findShiftId($request) {
        return Shift::where('time', $request->shift)->first()->id;
    }

    public function getAstShift(){
        return $this->hasMany('App\Assistant', 'shift_id');
    }

    public function altSchedule(){
        return $this->belongsTo('App\AltSchedule');
    }

    public static function validate($request) {
        $validator = Validator::make($request->all(), ['time' => 'required|max:7']);
        return $validator;
    }
}
