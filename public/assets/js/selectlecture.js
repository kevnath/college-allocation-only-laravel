$(document).ready(function(){
    $("#course").on("change", function(){
        $.ajax({
           url:"/lecture/getclass",
            method: "post",
            data:{
                course:$("#course").val(),
                _token: $("input[name=_token]").val()
            },
            success: function(result){
                var classes = JSON.parse(result);
                var temp = -1;
                $("#class").empty().append("<option value='0'>-- Class --</option>");
                $("#day").empty().append("<option value='0'>-- Day --</option>");
                $("#shift").empty().append("<option value='0'>-- Shift --</option>");
                for(var i=0; i<classes.length; i++){
                    if(temp != -1) {
                        if (classes[i].class == classes[temp].class) {
                            continue;
                        }
                    }
                    $("#class").append("<option value='" + classes[i].class + "'>" + classes[i].class.substring(classes[i].course_id.length + 1, classes[i].class.length) + "</option>");
                    temp = i;
                }
                $("#class").val($("#class option:first-child").val()).trigger("change");
                $("#day").val($("#day option:first-child").val()).trigger("change");
                $("#shift").val($("#shift option:first-child").val()).trigger("change");
            },
            error: function(error){
                console.log(error);
            }
        });
    });
    $("#class").on("change", function(){
        $.ajax({
            url:"lecture/getday",
            method: "post",
            data:{
                class:$("#class").val(),
                _token: $("input[name=_token]").val()
            },
            success: function(result){
                var days = JSON.parse(result);
                temp = -1;
                $("#day").empty().append("<option value='0'>-- Day --</option>");
                $("#shift").empty().append("<option value='0'>-- Shift --</option>");
                for(var i=0; i<days.length; i++){
                    if(temp != -1) {
                        if (days[i].schedule_day == days[temp].schedule_day) {
                            continue;
                        }
                    }
                    $("#day").append("<option value='"+days[i].schedule_day+"'>"+days[i].schedule_day+"</option>");
                    temp = i;
                }
                $("#day").val($("#day option:first-child").val()).trigger("change");
                $("#shift").val($("#shift option:first-child").val()).trigger("change");
            },
            error: function(error){
                console.log(error);
            }
        });
    });
    $("#day").on("change", function(){
        $.ajax({
            url:"lecture/getshift",
            method: "post",
            data:{
                days:$("#day").val(),
                class:$("#class").val(),
                _token: $("input[name=_token]").val()
            },
            success: function(result){
                var shifts = JSON.parse(result);
                $("#shift").empty().append("<option value='0'>-- Shift --</option>");
                for(var i=0; i<shifts.length; i++){
                    $("#shift").append("<option value='"+shifts[i].college_shift+"'>"+shifts[i].college_shift+"</option>");

                }
                $("#shift").val($("#shift option:first-child").val()).trigger("change");
            },
            error: function(error){
                console.log(error);
            }
        });
    });
});

